package com.example.models.transactions;
import com.example.models.Asset;
import com.example.models.Currency;

import javax.persistence.*;
@Entity
@Table(name = "dividend_transaction")
public class DividendTransaction{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "transaction_type")
    private String transactionType;
    @JoinColumn(name = "asset_id")
    @ManyToOne
    private Asset asset;
    @Column(name = "amount")
    private double amount;
    @JoinColumn(name = "currency_id")
    @ManyToOne
    private Currency currency;
    @Column(name = "enabled")
    private int enabled;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Asset getAsset() {
        return asset;
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }
}
