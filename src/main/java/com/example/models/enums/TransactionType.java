package com.example.models.enums;

public enum TransactionType {
    ASSET_TRANSACTION,
    CURRENCY_EXCHANGE,
    DIVIDEND_TRANSACTION,
    USER_TRANSACTION
}
