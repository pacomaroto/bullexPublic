package com.example.dtos;


import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class AssetDTO {
    @PositiveOrZero(message = "id should be positive or zero")
    private int id;
    private String name;
    @Size(min = 1, max = 25, message = "Asset abbr size should be between 1 and 25")
    private String abbreviation;
    @Size(min = 4, max = 25, message = "Asset type size should be between 4 and 25")
    private String assetType;

    public AssetDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }
}
