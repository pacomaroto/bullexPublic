package com.example.dtos.transactionDTOs;

import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class DividendTransactionDTO {
    @PositiveOrZero(message = "id should be positive or zero")
    private int id;
    @Size(min = 4, max = 25, message = "Transaction type size should be between 4 and 25")
    private String transactionType;
    @PositiveOrZero(message = "Asset id should be positive or zero")
    private int assetId;
    @PositiveOrZero(message = "This should be positive or zero")
    private double amount;
    @PositiveOrZero(message = "Currency id should be positive or zero")
    private int currencyId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }


    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getAssetId() {
        return assetId;
    }

    public void setAssetId(int assetId) {
        this.assetId = assetId;
    }

    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }
}
