package com.example.dtos.transactionDTOs;

import com.example.models.Currency;

import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class CurrencyExchangeDTO {
    @PositiveOrZero(message = "id should be positive or zero")
    private int id;
    @Size(min = 4, max = 25, message = "Transaction type size should be between 4 and 25")
    private String transactionType;
    @PositiveOrZero(message = "Currency id should be positive or zero")
    private int currencyFromId;
    @PositiveOrZero(message = "Amount exchange should be positive or zero")
    private double amountFrom;
    @PositiveOrZero(message = "Currency id should be positive or zero")
    private int currencyToId;
    @PositiveOrZero(message = "This should be positive or zero")
    private double amountTo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }


    public double getAmountFrom() {
        return amountFrom;
    }

    public void setAmountFrom(double amountFrom) {
        this.amountFrom = amountFrom;
    }


    public double getAmountTo() {
        return amountTo;
    }

    public void setAmountTo(double amountTo) {
        this.amountTo = amountTo;
    }

    public int getCurrencyFromId() {
        return currencyFromId;
    }

    public void setCurrencyFromId(int currencyFromId) {
        this.currencyFromId = currencyFromId;
    }

    public int getCurrencyToId() {
        return currencyToId;
    }

    public void setCurrencyToId(int currencyToId) {
        this.currencyToId = currencyToId;
    }
}
