package com.example.dtos.transactionDTOs;

import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class AssetTransactionDTO {
    @PositiveOrZero(message = "id should be positive or zero")
    int id;
    @Size(min = 4, max = 25, message = "Transaction type size should be between 4 and 25")
    private String transactionType;
    @PositiveOrZero(message = "Asset id should be positive or zero")
    private int assetId;
    @PositiveOrZero(message = "Asset shares should be positive or zero")
    private double shares;
    @PositiveOrZero(message = "Asset price per shares should be positive or zero")
    private double pricePerShare;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public int getAssetId() {
        return assetId;
    }

    public void setAssetId(int assetId) {
        this.assetId = assetId;
    }

    public double getShares() {
        return shares;
    }

    public void setShares(double shares) {
        this.shares = shares;
    }

    public double getPricePerShare() {
        return pricePerShare;
    }

    public void setPricePerShare(double pricePerShare) {
        this.pricePerShare = pricePerShare;
    }
}
