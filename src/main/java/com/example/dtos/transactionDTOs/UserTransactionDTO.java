package com.example.dtos.transactionDTOs;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class UserTransactionDTO {
    private int id;
    @Size(min = 4, max = 25, message = "Transaction type size should be between 4 and 25")
    private String transactionType;
    @PositiveOrZero(message = "User id must be positive or zero")
    private int userId;
    @NotNull
    private double amount;
    @PositiveOrZero(message = "Currency id must be positive or zero")
    private int currencyId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }
}
