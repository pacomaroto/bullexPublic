package com.example.dtos;

import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class CurrencyDTO {
    @PositiveOrZero(message = "id should be positive or zero")
    private int id;
    @Size(min = 2, max = 25, message = "Currency abbr size should be between 2 and 25")
    private String name;

    public CurrencyDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
