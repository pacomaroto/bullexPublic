package com.example.dtos.mappers;

import com.example.dtos.transactionDTOs.CurrencyExchangeDTO;
import com.example.models.enums.TransactionType;
import com.example.models.transactions.CurrencyExchange;
import com.example.services.contracts.CurrencyService;
import com.example.services.contracts.transactionService.CurrencyExchangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CurrencyExchangeMapper {
    private CurrencyExchangeService currencyExchangeService;
    private CurrencyService currencyService;

    @Autowired
    public CurrencyExchangeMapper(CurrencyExchangeService currencyExchangeService, CurrencyService currencyService) {
        this.currencyExchangeService = currencyExchangeService;
        this.currencyService = currencyService;
    }

    public CurrencyExchange mapFromDtoToCurrencyExchange(CurrencyExchange currencyExchange,
                                                         CurrencyExchangeDTO currencyExchangeDTO){
        currencyExchange.setTransactionType(String.valueOf(TransactionType.CURRENCY_EXCHANGE));
        currencyExchange.setCurrencyFrom(currencyService.getById(currencyExchangeDTO.getCurrencyFromId()));
        currencyExchange.setAmountFrom(currencyExchangeDTO.getAmountFrom());
        currencyExchange.setCurrencyTo(currencyService.getById(currencyExchangeDTO.getCurrencyToId()));
        currencyExchange.setAmountTo(currencyExchangeDTO.getAmountTo());
        currencyExchange.setEnabled(1);
        return currencyExchange;
    }
}
