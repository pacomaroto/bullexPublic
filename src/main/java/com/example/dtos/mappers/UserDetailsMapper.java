package com.example.dtos.mappers;

import com.example.dtos.UserDetailsDTO;
import com.example.models.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class UserDetailsMapper {
    public UserDetails fromUserDetailsDtoToUserDetails(UserDetailsDTO userDetailsDTO){
        UserDetails userDetails = new UserDetails();
        userDetails.setEmail(userDetailsDTO.getEmail());
        userDetails.setFirstName(userDetailsDTO.getFirstName());
        userDetails.setLastName(userDetailsDTO.getLastName());
        userDetails.setGender(userDetailsDTO.getGender());
        userDetails.setAge(userDetailsDTO.getAge());
        return userDetails;
    }
    public UserDetailsDTO fromUserDetailsToDto(UserDetails userDetails){
        UserDetailsDTO userDetailsDTO = new UserDetailsDTO();
        userDetailsDTO.setEmail(userDetails.getEmail());
        userDetailsDTO.setFirstName(userDetails.getFirstName());
        userDetailsDTO.setLastName(userDetails.getLastName());
        userDetailsDTO.setGender(userDetails.getGender());
        userDetailsDTO.setAge(userDetails.getAge());
        return userDetailsDTO;
    }
}
