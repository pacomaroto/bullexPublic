package com.example.dtos.mappers;

import com.example.dtos.transactionDTOs.UserTransactionDTO;
import com.example.models.enums.TransactionType;
import com.example.models.transactions.UserTransaction;
import com.example.services.contracts.CurrencyService;
import com.example.services.contracts.UserService;
import com.example.services.contracts.transactionService.UserTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserTransMapper {
    private UserTransactionService userTransactionService;
    private UserService userService;
    private CurrencyService currencyService;

    @Autowired
    public UserTransMapper(UserTransactionService userTransactionService, UserService userService, CurrencyService currencyService) {
        this.userTransactionService = userTransactionService;
        this.userService = userService;
        this.currencyService = currencyService;
    }

    public UserTransaction mapFromDtoToUserTransaction(UserTransaction userTransaction, UserTransactionDTO userTransactionDTO){
        userTransaction.setTransactionType(String.valueOf(TransactionType.USER_TRANSACTION));
        userTransaction.setAmount(userTransactionDTO.getAmount());
        userTransaction.setUser(userService.getById(userTransactionDTO.getUserId()));
        userTransaction.setCurrency(currencyService.getById(userTransactionDTO.getCurrencyId()));
        userTransaction.setEnabled(1);
        return userTransaction;
    }
}
