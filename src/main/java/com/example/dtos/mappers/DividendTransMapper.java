package com.example.dtos.mappers;

import com.example.dtos.transactionDTOs.DividendTransactionDTO;
import com.example.models.enums.TransactionType;
import com.example.models.transactions.DividendTransaction;
import com.example.services.contracts.AssetService;
import com.example.services.contracts.CurrencyService;
import com.example.services.contracts.transactionService.DividendTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DividendTransMapper {
    private DividendTransactionService dividendTransactionService;
    private AssetService assetService;
    private CurrencyService currencyService;

    @Autowired
    public DividendTransMapper(DividendTransactionService dividendTransactionService,
                               AssetService assetService, CurrencyService currencyService) {
        this.dividendTransactionService = dividendTransactionService;
        this.assetService = assetService;
        this.currencyService = currencyService;
    }
    public DividendTransaction mapFromDtoToDividendTransaction(DividendTransaction dividendTransaction,
                                                               DividendTransactionDTO dividendTransactionDTO){
        dividendTransaction.setTransactionType(String.valueOf(TransactionType.DIVIDEND_TRANSACTION));
        dividendTransaction.setAsset(assetService.getById(dividendTransactionDTO.getAssetId()));
        dividendTransaction.setAmount(dividendTransactionDTO.getAmount());
        dividendTransaction.setCurrency(currencyService.getById(dividendTransactionDTO.getCurrencyId()));
        dividendTransaction.setEnabled(1);
        return dividendTransaction;
    }
}
