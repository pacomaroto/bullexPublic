package com.example.dtos.mappers;

import com.example.dtos.transactionDTOs.AssetTransactionDTO;
import com.example.models.enums.TransactionType;
import com.example.models.transactions.AssetTransaction;
import com.example.services.contracts.AssetService;
import com.example.services.contracts.transactionService.AssetTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AssetTransMapper {
    private AssetTransactionService assetTransactionService;
    private AssetService assetService;
    @Autowired
    public AssetTransMapper(AssetTransactionService assetTransactionService, AssetService assetService) {
        this.assetTransactionService = assetTransactionService;
        this.assetService = assetService;
    }
    public AssetTransaction mapFormDtoToAssetTrans(AssetTransaction assetTransaction,
                                                   AssetTransactionDTO assetTransactionDTO){
        assetTransaction.setTransactionType(String.valueOf(TransactionType.ASSET_TRANSACTION));
        assetTransaction.setAsset(assetService.getById(assetTransactionDTO.getAssetId()));
        assetTransaction.setShares(assetTransactionDTO.getShares());
        assetTransaction.setPricePerShare(assetTransactionDTO.getPricePerShare());
        assetTransaction.setEnabled(1);
        return assetTransaction;
    }
}
