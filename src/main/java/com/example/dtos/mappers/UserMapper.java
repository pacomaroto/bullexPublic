package com.example.dtos.mappers;

import com.example.dtos.UserDTO;
import com.example.models.User;
import com.example.models.UserDetails;
import com.example.services.contracts.FileStorageService;
import com.example.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    private UserService userService;
    private FileStorageService fileStorageService;

    @Autowired
    public UserMapper(UserService userService, FileStorageService fileStorageService) {
        this.userService = userService;
        this.fileStorageService = fileStorageService;
    }


    public User mapFromDtoToUser(User user, UserDTO userDto){
        user.setUsername(userDto.getName());
        user.setPassword(userDto.getPassword());
        UserDetails userDetails = new UserDetails();
        userDetails.setEmail(userDto.getEmail());
        userDetails.setFirstName(userDto.getFirstName());
        userDetails.setLastName(userDto.getLastName());
        userDetails.setAge(userDto.getAge());
        userDetails.setGender(userDto.getGender());
//        userDetails.setPictureURL(fileStorageService.uploadFile(userDto.getFile(), userDto.getName()));
        userService.addDefaultUserDetails(user.getUsername());
        user.setUserDetails(userDetails);
        return user;
    }
}
