package com.example.dtos.mappers;

import com.example.dtos.AssetDTO;
import com.example.models.Asset;
import com.example.services.contracts.AssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AssetMapper {
    private AssetService assetService;

    @Autowired
    public AssetMapper(AssetService assetService) {
        this.assetService = assetService;
    }

    public Asset mapFromDtoToAsset(Asset asset, AssetDTO assetDTO){
        asset.setName(assetDTO.getName());
        asset.setAbbreviation(assetDTO.getAbbreviation());
        asset.setAssetType(assetDTO.getAssetType());
        asset.setEnabled(1);
        return asset;
    }
}
