package com.example.dtos.mappers;

import com.example.dtos.UserRegisterDTO;
import com.example.models.User;
import org.springframework.stereotype.Component;

@Component
public class UserRegisterMapper {

    public User fromUserRegisterDto(UserRegisterDTO userRegisterDto) {
        return new User(userRegisterDto.getUsername(), userRegisterDto.getPassword());
    }
}
