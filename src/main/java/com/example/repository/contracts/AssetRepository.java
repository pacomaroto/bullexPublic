package com.example.repository.contracts;

import com.example.models.Asset;

import java.util.List;

public interface AssetRepository {
    Asset getById(int id);

    List<Asset> getAll();

    void create(Asset asset);

    void update(Asset asset);

    void delete(Asset asset);
}
