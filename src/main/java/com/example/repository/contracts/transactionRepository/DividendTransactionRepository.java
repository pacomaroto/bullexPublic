package com.example.repository.contracts.transactionRepository;

import com.example.models.transactions.DividendTransaction;

import java.util.List;


public interface DividendTransactionRepository{
    DividendTransaction getById(int id);

    List<DividendTransaction> getAll();

    void create(DividendTransaction dividendTransaction);

    void update(DividendTransaction dividendTransaction);

    void delete(DividendTransaction dividendTransaction);
}
