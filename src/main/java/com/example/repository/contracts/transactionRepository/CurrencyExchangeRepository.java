package com.example.repository.contracts.transactionRepository;

import com.example.models.transactions.CurrencyExchange;

import java.util.List;

public interface CurrencyExchangeRepository {
    CurrencyExchange getById(int id);

    List<CurrencyExchange> getAll();

    void create(CurrencyExchange currencyExchange);

    void update(CurrencyExchange currencyExchange);

    void delete(CurrencyExchange currencyExchange);
}
