package com.example.repository.contracts.transactionRepository;
import com.example.models.transactions.AssetTransaction;

import java.util.List;

public interface AssetTransactionRepository {
    AssetTransaction getById(int id);

    List<AssetTransaction> getAll();

    void create(AssetTransaction assetTransaction);

    void update(AssetTransaction assetTransaction);

    void delete(AssetTransaction assetTransaction);
}
