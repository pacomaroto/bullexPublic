package com.example.repository.contracts.transactionRepository;

import com.example.models.transactions.UserTransaction;

import java.util.List;

public interface UserTransactionRepository {
    UserTransaction getById(int id);

    List<UserTransaction> getAll();

    void create(UserTransaction userTransaction);

    void update(UserTransaction userTransaction);

    void delete(UserTransaction userTransaction);
}
