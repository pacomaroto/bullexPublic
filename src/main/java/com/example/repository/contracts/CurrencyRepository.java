package com.example.repository.contracts;

import com.example.models.Currency;

import java.util.List;

public interface CurrencyRepository {
    Currency getById(int id);

    List<Currency> getAll();

    void create(Currency currency);

    void update(Currency currency);

    void delete(Currency currency);
}
