package com.example.repository.contracts;

import com.example.models.User;
import com.example.models.UserDetails;


import java.util.List;

public interface UserRepository {
    User getById(int id);

    List<User> getAll();

    void create(User user);

    void update(User user);

    void delete(User user);

    User getUserByUsername(String username);

    void createUserDetails(int id, UserDetails userDetails);

    void updateUserDetails(UserDetails userDetails);
}
