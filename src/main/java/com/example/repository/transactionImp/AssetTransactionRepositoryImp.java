package com.example.repository.transactionImp;
import com.example.models.transactions.AssetTransaction;
import com.example.repository.contracts.transactionRepository.AssetTransactionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static java.lang.String.format;

@Repository
public class AssetTransactionRepositoryImp implements AssetTransactionRepository {
    SessionFactory sessionFactory;

    public AssetTransactionRepositoryImp(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(AssetTransaction assetTransaction) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(assetTransaction);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(AssetTransaction assetTransaction) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(assetTransaction);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(AssetTransaction assetTransaction) {
        assetTransaction.setEnabled(0);
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(assetTransaction);
            session.getTransaction().commit();
        }
    }

    @Override
    public AssetTransaction getById(int id) {
        try(Session session = sessionFactory.openSession()){
            AssetTransaction assetTransaction = session.get(AssetTransaction.class, id);
            if(assetTransaction == null){
                throw new EntityNotFoundException(format("exception", id));
            }
            return assetTransaction;
        }
    }

    @Override
    public List<AssetTransaction> getAll() {
        try(Session session = sessionFactory.openSession()){
            return session.createQuery("from AssetTransaction where enabled = 1", AssetTransaction.class).getResultList();
        }
    }
}
