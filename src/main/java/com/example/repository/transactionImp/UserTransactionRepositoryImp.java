package com.example.repository.transactionImp;

import com.example.models.transactions.UserTransaction;
import com.example.repository.contracts.transactionRepository.UserTransactionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.List;
@Repository
public class UserTransactionRepositoryImp implements UserTransactionRepository {
    SessionFactory sessionFactory;

    public UserTransactionRepositoryImp(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public UserTransaction getById(int id) {
        try(Session session = sessionFactory.openSession()){
            UserTransaction userTransaction = session.get(UserTransaction.class, id);
            if (userTransaction == null){
                throw new EntityNotFoundException(String.format("exception", id));
            }
            return userTransaction;
        }
    }

    @Override
    public List<UserTransaction> getAll() {
        try(Session session = sessionFactory.openSession()){
            return session.createQuery("from UserTransaction where enabled = 1", UserTransaction.class).getResultList();
        }
    }

    @Override
    public void create(UserTransaction userTransaction) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(userTransaction);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(UserTransaction userTransaction) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(userTransaction);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(UserTransaction userTransaction) {
        userTransaction.setEnabled(0);
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(userTransaction);
            session.getTransaction().commit();
        }
    }
}
