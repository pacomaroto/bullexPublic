package com.example.repository.transactionImp;

import com.example.models.transactions.CurrencyExchange;
import com.example.repository.contracts.transactionRepository.CurrencyExchangeRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.List;
@Repository
public class CurrencyExchangeRepositoryImp implements CurrencyExchangeRepository {
    SessionFactory sessionFactory;

    public CurrencyExchangeRepositoryImp(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public CurrencyExchange getById(int id) {
        try(Session session = sessionFactory.openSession()){
            CurrencyExchange currencyExchange = session.get(CurrencyExchange.class, id);
            if (currencyExchange == null){
                throw new EntityNotFoundException(String.format("exception", id));
            }
            return currencyExchange;
        }

    }

    @Override
    public List<CurrencyExchange> getAll() {
        try(Session session = sessionFactory.openSession()) {
            return session.createQuery("from CurrencyExchange where enabled = 1", CurrencyExchange.class)
                    .getResultList();
        }
    }

    @Override
    public void create(CurrencyExchange currencyExchange) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(currencyExchange);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(CurrencyExchange currencyExchange) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(currencyExchange);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(CurrencyExchange currencyExchange) {
        currencyExchange.setEnabled(0);
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(currencyExchange);
            session.getTransaction().commit();
        }
    }

}
