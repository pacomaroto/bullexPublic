package com.example.repository.transactionImp;

import com.example.models.transactions.DividendTransaction;
import com.example.repository.contracts.transactionRepository.DividendTransactionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.List;
@Repository
public class DividendTransactionRepositoryImp implements DividendTransactionRepository {
    SessionFactory sessionFactory;

    public DividendTransactionRepositoryImp(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public DividendTransaction getById(int id) {
        try(Session session = sessionFactory.openSession()){
            DividendTransaction dividendTransaction = session.get(DividendTransaction.class, id);
            if (dividendTransaction == null){
                throw new EntityNotFoundException(String.format("exception", id));
            }
        return dividendTransaction;
        }
    }

    @Override
    public List<DividendTransaction> getAll() {
        try(Session session = sessionFactory.openSession()){
        return session.createQuery("from DividendTransaction where enabled = 1", DividendTransaction.class)
                .getResultList();
        }
    }

    @Override
    public void create(DividendTransaction dividendTransaction) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(dividendTransaction);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(DividendTransaction dividendTransaction) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(dividendTransaction);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(DividendTransaction dividendTransaction) {
        dividendTransaction.setEnabled(0);
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(dividendTransaction);
            session.getTransaction().commit();
        }
    }

}
