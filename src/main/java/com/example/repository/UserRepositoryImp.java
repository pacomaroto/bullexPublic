package com.example.repository;


import com.example.models.User;
import com.example.models.UserDetails;
import com.example.repository.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static java.lang.String.format;

@Repository
public class UserRepositoryImp implements UserRepository {
    private SessionFactory sessionFactory;

    public UserRepositoryImp(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public User getById(int id) {
        try(Session session = sessionFactory.openSession()){
            User user = session.get(User.class, id);
            if(user == null){
                throw new EntityNotFoundException(format("exception", id));
            }
            return user;
        }
    }

    @Override
    public User getUserByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {

            Query<User> query = session.createQuery("from User where username = :name");
            query.setParameter("name", username);
            User user = query.getSingleResult();
            if (user == null) {
                throw new EntityNotFoundException(String.format("User with username = %s does not exist", username));
            }
            return user;
        }
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> userQuery = session.createQuery("from User where enabled = 1", User.class);
            return userQuery.getResultList();
        }
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(User user) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }
    @Override
    public void createUserDetails(int id, UserDetails userDetails) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(userDetails);
            session.getTransaction().commit();
        }
        User user = getById(id);
        user.setUserDetails(userDetails);
        update(user);
        }
    @Override
    public void updateUserDetails(UserDetails userDetails) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(userDetails);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(User user) {
        user.setEnabled(0);
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }
}
