package com.example.repository;

import com.example.models.Asset;
import com.example.repository.contracts.AssetRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static java.lang.String.format;

@Repository
public class AssetRepositoryImp implements AssetRepository {
    SessionFactory sessionFactory;

    public AssetRepositoryImp(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Asset getById(int id) {
        try(Session session = sessionFactory.openSession()){
            Asset asset = session.get(Asset.class, id);
            if(asset == null){
                throw new EntityNotFoundException(format("exception", id));
            }
            return asset;
        }
    }

    @Override
    public List<Asset> getAll() {
        try(Session session = sessionFactory.openSession()){
            return session.createQuery("from Asset where enabled = 1", Asset.class)
                    .getResultList();
        }
    }

    @Override
    public void create(Asset asset) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(asset);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Asset asset) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(asset);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Asset asset) {
        asset.setEnabled(0);
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(asset);
            session.getTransaction().commit();
        }
    }
}
