package com.example.repository;

import com.example.models.Currency;
import com.example.repository.contracts.CurrencyRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static java.lang.String.format;
@Repository
public class CurrencyRepositoryImp implements CurrencyRepository {
    SessionFactory sessionFactory;

    public CurrencyRepositoryImp(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Currency getById(int id) {
        try(Session session = sessionFactory.openSession()){
            Currency currency = session.get(Currency.class, id);
            if(currency == null){
                throw new EntityNotFoundException(format("exception", id));
            }
            return currency;
        }

    }

    @Override
    public List<Currency> getAll() {
        try(Session session = sessionFactory.openSession()){
            Query<Currency> currencyQuery = session.createQuery("from Currency where enabled = 1", Currency.class);
            return currencyQuery.getResultList();
        }
    }

    @Override
    public void create(Currency currency) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(currency);
            session.getTransaction().commit();
        }

    }

    @Override
    public void update(Currency currency) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(currency);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Currency currency) {
        currency.setEnabled(0);
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(currency);
            session.getTransaction().commit();
        }
    }
}
