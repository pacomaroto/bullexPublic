package com.example.bullex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages = {"com.example.models","com.example.repository","com.example.services","com.example.controllers"
,"com.example.configuration", "com.example.dtos", "com.example.message"})
public class BullexApplication {
    public static void main(String[] args) {
        SpringApplication.run(BullexApplication.class, args);
    }
}
