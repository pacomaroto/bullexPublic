package com.example.controllers;
import com.example.dtos.UserDTO;
import com.example.dtos.mappers.UserMapper;
import com.example.models.User;
import com.example.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class UsersController {
    private UserService userService;
    private UserMapper mapper;

    @Autowired
    public UsersController(UserService userService, UserMapper mapper) {
        this.userService = userService;
        this.mapper = mapper;
    }

    @GetMapping("/users")
    public ModelAndView showUsers(){
        ModelAndView mav = new ModelAndView("users");
        mav.addObject("users", userService.getAll());
        return mav;
    }

//    @GetMapping("/users/new")
//    public String showNewUserForm(Model model) {
//        model.addAttribute("user", new UserDTO());
//        return "user";
//    }
//
//    @PostMapping("/users/new")
//    public String createUser(@Valid @ModelAttribute("user") UserDTO userDto, BindingResult errors) {
//        if (errors.hasErrors()){
//            return "user";
//        }
//        User user = new User();
//        userService.create(mapper.mapFromDtoToUser(user, userDto));
//        return "redirect:/users";
//    }

    @PostMapping("/users/delete/{name}")
    public String deleteUser(@PathVariable String name) {
        User user = userService.getUserByUsername(name);
        userService.delete(user);

        return "redirect:/users";
    }
}
