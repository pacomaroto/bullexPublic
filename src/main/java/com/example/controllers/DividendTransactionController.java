package com.example.controllers;

import com.example.dtos.mappers.DividendTransMapper;
import com.example.dtos.transactionDTOs.DividendTransactionDTO;
import com.example.models.User;
import com.example.models.transactions.DividendTransaction;
import com.example.services.contracts.AssetService;
import com.example.services.contracts.CurrencyService;
import com.example.services.contracts.transactionService.DividendTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class DividendTransactionController {
    private DividendTransactionService dividendTransactionService;
    private AssetService assetService;
    private CurrencyService currencyService;
    private DividendTransMapper mapper;

    @Autowired
    public DividendTransactionController(DividendTransactionService dividendTransactionService,
                                         AssetService assetService, CurrencyService currencyService,
                                         DividendTransMapper mapper) {
        this.dividendTransactionService = dividendTransactionService;
        this.assetService = assetService;
        this.currencyService = currencyService;
        this.mapper = mapper;
    }

    @GetMapping("/dividendTransactions")
    public String showDividendTransactions(Model model) {
        model.addAttribute("dividendTransactions", dividendTransactionService.getAll());
        return "dividendTransactions";
    }

    @GetMapping("/dividendTransactions/new")
    public String showNewDividendTransactions(Model model) {
        model.addAttribute("dividendTransaction", new DividendTransactionDTO());
        model.addAttribute("assets", assetService.getAll());
        model.addAttribute("currencies", currencyService.getAll());
        return "dividendTransaction";
    }

    @PostMapping("/dividendTransactions/new")
    public String createDividendTransaction(@Valid @ModelAttribute("dividendTransaction")
                                                    DividendTransactionDTO dividendTransactionDTO, BindingResult errors) {
        if (errors.hasErrors()) {
            return "dividendTransaction";
        }
        DividendTransaction dividendTransaction = new DividendTransaction();
        dividendTransactionService.create(mapper.mapFromDtoToDividendTransaction(dividendTransaction, dividendTransactionDTO));
        return "redirect:/dividendTransactions";
    }
    @PostMapping("/dividendTransactions/delete/{id}")
    public String deleteDividendTransaction(@PathVariable int id) {
        DividendTransaction dividendTransaction = dividendTransactionService.getById(id);
        dividendTransactionService.delete(dividendTransaction);
        return "redirect:/dividendTransactions";
    }
}
