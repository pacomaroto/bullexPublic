package com.example.controllers;

import com.example.dtos.mappers.AssetTransMapper;
import com.example.dtos.transactionDTOs.AssetTransactionDTO;
import com.example.models.transactions.AssetTransaction;
import com.example.models.transactions.DividendTransaction;
import com.example.services.contracts.AssetService;
import com.example.services.contracts.transactionService.AssetTransactionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class AssetTransactionsController {
    private AssetTransactionService assetTransactionService;
    private AssetService assetService;
    private AssetTransMapper mapper;

    public AssetTransactionsController(AssetTransactionService assetTransactionService, AssetService assetService,
                                       AssetTransMapper mapper) {
        this.assetTransactionService = assetTransactionService;
        this.assetService = assetService;
        this.mapper = mapper;
    }
    @GetMapping("/assetTransactions")
    public String showAssetTransactions(Model model){
        model.addAttribute("assetTransactions" , assetTransactionService.getAll());
        return "assetTransactions";
    }
    @GetMapping("/assetTransactions/new")
    public String showNewAssetTransactions(Model model) {
        model.addAttribute("assetTransaction", new AssetTransactionDTO());
        model.addAttribute("assets", assetService.getAll());
        return "assetTransaction";
    }

    @PostMapping("/assetTransactions/new")
    public String createAssetTransaction(@Valid @ModelAttribute("assetTransaction")
                                                    AssetTransactionDTO assetTransactionDTO, BindingResult errors) {
        if (errors.hasErrors()){
            return "assetTransaction";
        }
        AssetTransaction assetTransaction = new AssetTransaction();
        assetTransactionService.create(mapper.mapFormDtoToAssetTrans(assetTransaction, assetTransactionDTO));
        return "redirect:/assetTransactions";
    }
    @PostMapping("/asssetTransactions/delete/{id}")
    public String deleteAssetTransaction(@PathVariable int id) {
        AssetTransaction assetTransaction = assetTransactionService.getById(id);
        assetTransactionService.delete(assetTransaction);
        return "redirect:/assetTransactions";
    }
}
