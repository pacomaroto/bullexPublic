package com.example.controllers;

import com.example.dtos.AssetDTO;
import com.example.dtos.mappers.AssetMapper;
import com.example.models.Asset;
import com.example.models.transactions.DividendTransaction;
import com.example.services.contracts.AssetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class AssetsController {
    private AssetService assetService;
    private AssetMapper mapper;

    @Autowired
    public AssetsController(AssetService assetService, AssetMapper mapper) {
        this.assetService = assetService;
        this.mapper = mapper;
    }
    @GetMapping("/assets")
    public String showAllAssets(Model model){
        model.addAttribute("assets", assetService.getAll());
        return "assets";
    }
    @GetMapping("/assets/new")
    public String showNewAssetForm(Model model) {
        model.addAttribute("asset", new Asset());
        return "asset";
    }

    @PostMapping("/assets/new")
    public String createAsset(@Valid @ModelAttribute("asset") AssetDTO assetDTO, BindingResult errors) {
        if (errors.hasErrors()){
            return "asset";
        }
        Asset asset = new Asset();
        assetService.create(mapper.mapFromDtoToAsset(asset, assetDTO));
        return "redirect:/assets";
    }
    @PostMapping("/asset/delete/{id}")
    public String deleteDividendTransaction(@PathVariable int id) {
        Asset asset = assetService.getById(id);
        assetService.delete(asset);
        return "redirect:/dividendTransactions";
    }
}
