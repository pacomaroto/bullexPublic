package com.example.controllers.rest;

import com.example.dtos.CurrencyDTO;
import com.example.models.Currency;
import com.example.services.contracts.CurrencyService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/currency")
public class CurrencyRestController {
    private CurrencyService currencyService;

    public CurrencyRestController(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @GetMapping("/all")
    public List<Currency> returnCurrency() {
        return currencyService.getAll();
    }

    @GetMapping("/{id}")
    public Currency currencyGetById(@PathVariable int id) {
        return currencyService.getById(id);
    }

    @PostMapping
    public Currency createUser(@RequestBody @Valid CurrencyDTO currencyDto) {
        try {
            Currency currency = new Currency();
            currency.setName(currencyDto.getName());
            currencyService.create(currency);
            return currency;
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/{id}")
    public Currency updateCurrency(@PathVariable int id, @RequestBody @Valid CurrencyDTO currencyDto) {
        try {
            Currency currency = currencyService.getById(id);
            currency.setName(currencyDto.getName());
            currencyService.update(currency);
            return currency;
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
