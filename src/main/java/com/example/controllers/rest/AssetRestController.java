package com.example.controllers.rest;

import com.example.dtos.AssetDTO;
import com.example.dtos.mappers.AssetMapper;
import com.example.models.Asset;
import com.example.services.contracts.AssetService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/assets")
public class AssetRestController {
    private AssetService assetService;
    private AssetMapper mapper;

    public AssetRestController(AssetService assetService, AssetMapper mapper) {
        this.assetService = assetService;
        this.mapper = mapper;
    }

    @GetMapping("/all")
    public List<Asset> returnAssets() {
        return assetService.getAll();
    }

    @GetMapping("/{id}")
    public Asset assetGetById(@PathVariable int id) {
        return assetService.getById(id);
    }

    @PostMapping("/assetStock")
    public Asset createAsset(@RequestBody @Valid AssetDTO assetDto) {
        try {
            Asset asset = new Asset();
            assetService.create(mapper.mapFromDtoToAsset(asset, assetDto));
            return asset;
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/{id}")
    public Asset updateAsset(@PathVariable int id, @RequestBody @Valid AssetDTO assetDto) {
        try {
            Asset asset = assetService.getById(id);
            assetService.update(mapper.mapFromDtoToAsset(asset, assetDto));
            return asset;
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
