package com.example.controllers.rest;


import com.example.dtos.UserDTO;
import com.example.dtos.UserDetailsDTO;
import com.example.dtos.mappers.UserDetailsMapper;
import com.example.dtos.mappers.UserMapper;
import com.example.exceptions.EntityNotFoundException;
import com.example.models.User;
import com.example.models.UserDetails;
import com.example.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserRestController {
    private UserService userService;
    private final UserMapper mapper;
    private final UserDetailsMapper detailsMapper;

    @Autowired
    public UserRestController(UserService userService, UserMapper mapper, UserDetailsMapper detailsMapper) {
        this.userService = userService;
        this.mapper = mapper;
        this.detailsMapper = detailsMapper;
    }

    @GetMapping("/all")
    public List<User> returnUsers() {
        return userService.getAll();
    }

    @GetMapping("/{id}")
    public User userGetById(@PathVariable int id) {
        return userService.getById(id);
    }
    @GetMapping("/username")
    public User userGetByUsername(@RequestParam String username){
        return userService.getUserByUsername(username);
    }
    @PostMapping
    public User createUser(@RequestBody @Valid UserDTO userDto) {
        try {
            User user = new User();
            userService.create(mapper.mapFromDtoToUser(user, userDto));
            return user;
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
//    @PutMapping("/{id}")
//    public User updateUser(@PathVariable int id, @RequestBody @Valid UserDTO userDto){
//        try  {
//            User user = userService.getById(id);
//            user.setUsername(userDto.getName());
//            userService.update(user);
//            return user;
//        } catch (IllegalArgumentException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        }
//    }
    @PostMapping("/{id}")
    public User updateUserDetails(@PathVariable int id,
                                  @RequestBody @Valid UserDetailsDTO userDetailsDTO){
        try {
            User user = userService.getById(id);
            userService.updateUserDetails(id, userDetailsDTO);
            return user;
        } catch (IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @DeleteMapping
    public void deleteUser(@RequestBody User user) {
        try {
            userService.delete(user);
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

}
