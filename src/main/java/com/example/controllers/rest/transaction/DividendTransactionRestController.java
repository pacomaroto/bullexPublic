package com.example.controllers.rest.transaction;

import com.example.dtos.mappers.DividendTransMapper;
import com.example.dtos.transactionDTOs.DividendTransactionDTO;
import com.example.models.enums.TransactionType;
import com.example.models.transactions.DividendTransaction;
import com.example.services.contracts.AssetService;
import com.example.services.contracts.CurrencyService;
import com.example.services.contracts.transactionService.DividendTransactionService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/dividendTransaction")
public class DividendTransactionRestController {
    private DividendTransactionService dividendTransactionService;
    private CurrencyService currencyService;
    private AssetService assetService;
    private DividendTransMapper mapper;

    public DividendTransactionRestController(DividendTransactionService dividendTransactionService,
                                             CurrencyService currencyService, AssetService assetService,
                                             DividendTransMapper mapper) {
        this.dividendTransactionService = dividendTransactionService;
        this.currencyService = currencyService;
        this.assetService = assetService;
        this.mapper = mapper;
    }
    @GetMapping("/all")
    public List<DividendTransaction> returnAllDividendTransactions(){
        return dividendTransactionService.getAll();
    }
    @GetMapping("/{id}")
    public DividendTransaction DividendTransactionGetById(@PathVariable int id){
        return dividendTransactionService.getById(id);
    }
    @PostMapping
    public DividendTransaction createDividendTransaction(@RequestBody @Valid DividendTransactionDTO dividendTransactionDTO){
        try {
            DividendTransaction dividendTransaction = new DividendTransaction();
            dividendTransactionService.create(mapper.mapFromDtoToDividendTransaction(dividendTransaction, dividendTransactionDTO));
            return dividendTransaction;
        }catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @PutMapping("/{id}")
    public DividendTransaction updateDividendTransaction(@PathVariable int id, @RequestBody @Valid DividendTransactionDTO dividendTransactionDTO){
        try {
            DividendTransaction dividendTransaction = dividendTransactionService.getById(id);
            dividendTransactionService.update(mapper.mapFromDtoToDividendTransaction(dividendTransaction, dividendTransactionDTO));
            return dividendTransaction;

        }catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
    }
}
}
