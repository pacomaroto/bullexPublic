package com.example.controllers.rest.transaction;
import com.example.dtos.mappers.AssetTransMapper;
import com.example.dtos.transactionDTOs.AssetTransactionDTO;
import com.example.models.transactions.AssetTransaction;
import com.example.services.contracts.AssetService;
import com.example.services.contracts.transactionService.AssetTransactionService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/assetTransaction")
public class AssetTransactionRestController {
    private AssetTransactionService assetTransactionService;
    private AssetService assetService;
    private AssetTransMapper mapper;

    public AssetTransactionRestController(AssetTransactionService assetTransactionService, AssetService assetService,
                                          AssetTransMapper mapper) {
        this.assetTransactionService = assetTransactionService;
        this.assetService = assetService;
        this.mapper = mapper;
    }

    @GetMapping("/all")
    public List<AssetTransaction> returnAssetTransactions() {
        return assetTransactionService.getAll();
    }

    @GetMapping("/{id}")
    public AssetTransaction assetTransactionGetById(@PathVariable int id) {
        return assetTransactionService.getById(id);
    }

    @PostMapping
    public AssetTransaction createAssetTransaction(@RequestBody @Valid AssetTransactionDTO assetTransactionDto) {
        try {
            AssetTransaction assetTransaction = new AssetTransaction();
            assetTransactionService.create(mapper.mapFormDtoToAssetTrans(assetTransaction, assetTransactionDto));
            return assetTransaction;
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }


    @PutMapping("/{id}")
    public AssetTransaction updateAssetTransaction(@PathVariable int id, @RequestBody @Valid AssetTransactionDTO assetTransactionDto) {
        try {
            AssetTransaction assetTransaction = assetTransactionService.getById(id);
            assetTransactionService.update(mapper.mapFormDtoToAssetTrans(assetTransaction, assetTransactionDto));
            return assetTransaction;
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
