package com.example.controllers.rest.transaction;

import com.example.dtos.mappers.UserTransMapper;
import com.example.dtos.transactionDTOs.UserTransactionDTO;
import com.example.models.transactions.UserTransaction;
import com.example.services.contracts.CurrencyService;
import com.example.services.contracts.UserService;
import com.example.services.contracts.transactionService.UserTransactionService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/userTransaction")
public class UserTransactionRestController {
    private UserTransactionService userTransactionService;
    private UserService userService;
    private CurrencyService currencyService;
    private UserTransMapper mapper;

    public UserTransactionRestController(UserTransactionService userTransactionService, UserService userService,
                                         CurrencyService currencyService, UserTransMapper mapper) {
        this.userTransactionService = userTransactionService;
        this.userService = userService;
        this.currencyService = currencyService;
        this.mapper = mapper;
    }

    @GetMapping("/all")
    public List<UserTransaction> returnAllUserTransactions() {
        return userTransactionService.getAll();
    }

    @GetMapping("/{id}")
    public UserTransaction getUserTransactionById(@PathVariable int id) {
        return userTransactionService.getById(id);
    }

    @PostMapping
    public UserTransaction createUserTransaction(@RequestBody @Valid UserTransactionDTO userTransactionDTO) {
        try {
            UserTransaction userTransaction = new UserTransaction();
            userTransactionService.create(mapper.mapFromDtoToUserTransaction(userTransaction, userTransactionDTO));
            return userTransaction;
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @PostMapping("/{id}")
    public UserTransaction updateUserTransaction(@PathVariable int id, @RequestBody @Valid UserTransactionDTO userTransactionDTO){
        try {
            UserTransaction userTransaction = userTransactionService.getById(id);
            userTransactionService.update(mapper.mapFromDtoToUserTransaction(userTransaction, userTransactionDTO));
            return userTransaction;
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
