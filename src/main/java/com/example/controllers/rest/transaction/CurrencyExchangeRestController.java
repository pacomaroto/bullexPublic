package com.example.controllers.rest.transaction;

import com.example.dtos.mappers.CurrencyExchangeMapper;
import com.example.dtos.transactionDTOs.CurrencyExchangeDTO;
import com.example.models.transactions.CurrencyExchange;
import com.example.services.contracts.CurrencyService;
import com.example.services.contracts.transactionService.CurrencyExchangeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/currencyExchange")
public class CurrencyExchangeRestController {
    private CurrencyExchangeService currencyExchangeService;
    private CurrencyService currencyService;
    private CurrencyExchangeMapper mapper;

    public CurrencyExchangeRestController(CurrencyExchangeService currencyExchangeService,
                                          CurrencyService currencyService,
                                          CurrencyExchangeMapper mapper) {
        this.currencyExchangeService = currencyExchangeService;
        this.currencyService = currencyService;
        this.mapper = mapper;
    }
    @GetMapping("/all")
    public List<CurrencyExchange> returnAllCurrencyExchange(){
        return currencyExchangeService.getAll();
    }
    @GetMapping("/{id}")
    public CurrencyExchange currencyExchangeGetById(@PathVariable int id){
        return currencyExchangeService.getById(id);
    }
    @PostMapping
    public CurrencyExchange createCurrencyExchange(@RequestBody @Valid CurrencyExchangeDTO currencyExchangeDTO){
        try{
            CurrencyExchange currencyExchange = new CurrencyExchange();
            currencyExchangeService.create(mapper.mapFromDtoToCurrencyExchange(currencyExchange, currencyExchangeDTO));
            return currencyExchange;
        }catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @PutMapping("/{id}")
    public CurrencyExchange updateCurrencyExchange(@PathVariable int id, @RequestBody @Valid CurrencyExchangeDTO currencyExchangeDTO) {
        try {
            CurrencyExchange currencyExchange = currencyExchangeService.getById(id);
            currencyExchangeService.update(mapper.mapFromDtoToCurrencyExchange(currencyExchange, currencyExchangeDTO));
            return currencyExchange;
        }catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
