package com.example.controllers;

import com.example.dtos.mappers.CurrencyExchangeMapper;
import com.example.dtos.transactionDTOs.CurrencyExchangeDTO;
import com.example.models.transactions.CurrencyExchange;
import com.example.services.contracts.CurrencyService;
import com.example.services.contracts.transactionService.CurrencyExchangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class CurrencyExchangesController {
    private CurrencyExchangeService currencyExchangeService;
    private CurrencyService currencyService;
    private CurrencyExchangeMapper mapper;

    @Autowired
    public CurrencyExchangesController(CurrencyExchangeService currencyExchangeService,
                                       CurrencyService currencyService, CurrencyExchangeMapper mapper) {
        this.currencyExchangeService = currencyExchangeService;
        this.currencyService = currencyService;
        this.mapper = mapper;
    }
    @GetMapping("/currencyExchanges")
    public String showCurrencyExchanges(Model model){
        model.addAttribute("currencyExchanges", currencyExchangeService.getAll());
        return "currencyExchanges";
    }
    @GetMapping("/currencyExchanges/new")
    public String showNewCurrencyExchange(Model model){
        model.addAttribute("currencyExchange", new CurrencyExchangeDTO());
        model.addAttribute("currencies", currencyService.getAll());
        model.addAttribute("currenciess", currencyService.getAll());
        return "currencyExchange";
    }
    @PostMapping("/currencyExchanges/new")
    public String createNewCurrencyExchange(@Valid @ModelAttribute("currencyExchange") CurrencyExchangeDTO currencyExchangeDTO,
                                            BindingResult errors){
        if (errors.hasErrors()){
            return "currencyExchange";
        }
        CurrencyExchange currencyExchange = new CurrencyExchange();
        currencyExchangeService.create(mapper.mapFromDtoToCurrencyExchange(currencyExchange, currencyExchangeDTO));
        return "redirect:/currencyExchanges";
    }
    @PostMapping("/currencyExchanges/delete/{id}")
    public String deleteCurrencyExchanges(@PathVariable int id) {
        CurrencyExchange currencyExchange = currencyExchangeService.getById(id);
        currencyExchangeService.delete(currencyExchange);
        return "redirect:/currencyExchanges";
    }
}
