package com.example.controllers;

import com.example.dtos.UserRegisterDTO;
import com.example.dtos.mappers.UserRegisterMapper;
import com.example.models.User;
import com.example.models.UserDetails;
import com.example.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

@Controller
public class RegisterController {
    private final UserDetailsManager userDetailsManager;
    private final PasswordEncoder passwordEncoder;
    private final UserRegisterMapper mapper;
    private final UserService userService;

    @Autowired
    public RegisterController(UserDetailsManager userDetailsManager,
                              PasswordEncoder passwordEncoder, UserRegisterMapper mapper,
                              UserService userService) {
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.mapper = mapper;
        this.userService = userService;
    }
    @GetMapping("/users/new")
    public String showNewUserForm(Model model) {
        model.addAttribute("user", new UserRegisterDTO());
        return "user";
    }

    @PostMapping("/users/new")
    public ModelAndView createUser(@Valid UserRegisterDTO userDto, BindingResult bindingResult) {
        ModelAndView mav = new ModelAndView();
        if (bindingResult.hasErrors()) {
            mav.setViewName("user");
            mav.addObject("user", new UserRegisterDTO());
            return mav;
        }
        if (userDetailsManager.userExists(userDto.getUsername())) {
            mav.addObject("error", "User with the same username already exists!");
            mav.addObject("user", new UserRegisterDTO());
            mav.setViewName("user");
            return mav;
        }
        if (!userDto.getPassword().equals(userDto.getPasswordConfirmation())) {
            mav.addObject("error", "Password doesn't match!");
            mav.addObject("user", new UserRegisterDTO());
            mav.setViewName("user");
            return mav;
        }
        mav.setViewName("index");
        User user = mapper.fromUserRegisterDto(userDto);
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser = new org.springframework.security.core.userdetails.User(
                user.getUsername(), passwordEncoder.encode(user.getPassword()), authorities);
        userDetailsManager.createUser(newUser);
        userService.addDefaultUserDetails(user.getUsername());
        return mav;
    }
}
