package com.example.controllers.errors;

import com.example.exceptions.EntityNotFoundException;
import com.example.message.ResponseMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import java.io.PrintWriter;
import java.io.StringWriter;

@ControllerAdvice("com.example.controllers.rest")
public class CustomControllerAdvice {

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleCustomDataNotFoundExceptions(
            Exception e) {
       return generateResponse(HttpStatus.NOT_FOUND, e);
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<ResponseMessage> handleMaxSizeException(MaxUploadSizeExceededException exc) {
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage("File too large!"));
    }


    private String getStackTrace(Exception e){
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        e.printStackTrace(printWriter);
        String stackTrace = stringWriter.toString();
        return stackTrace;
    }

    private ResponseEntity<ErrorResponse> generateResponseWithStackTrace
            (HttpStatus status, Exception e){
        ErrorResponse errorResponse =
                new ErrorResponse(status, e.getMessage(), getStackTrace(e));
        return new ResponseEntity<>(errorResponse, status);
    }

    private ResponseEntity<ErrorResponse> generateResponse
            (HttpStatus status, Exception e){
        ErrorResponse errorResponse =
                new ErrorResponse(status, e.getMessage());
        return new ResponseEntity<>(errorResponse, status);
    }
}
