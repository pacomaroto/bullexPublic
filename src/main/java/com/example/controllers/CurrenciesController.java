package com.example.controllers;

import com.example.dtos.CurrencyDTO;
import com.example.models.Currency;
import com.example.models.transactions.DividendTransaction;
import com.example.services.contracts.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class CurrenciesController {
    private CurrencyService currencyService;

    @Autowired
    public CurrenciesController(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @GetMapping("/currencies")
    public String showCurrencies(Model model) {
        model.addAttribute("currencies", currencyService.getAll());
        return "currencies";
    }

    @GetMapping("/currencies/new")
    public String showNewCurrencyForm(Model model) {
        model.addAttribute("currency", new Currency());
        return "currency";
    }

    @PostMapping("/currencies/new")
    public String createCurrency(@Valid @ModelAttribute("currency") CurrencyDTO currencyDTO, BindingResult errors) {
        if (errors.hasErrors()) {
            return "currency";
        }
        Currency currency = new Currency();
        currency.setName(currencyDTO.getName());
        currency.setEnabled(1);
        currencyService.create(currency);
        return "redirect:/currencies";
    }
    @PostMapping("/currencies/delete/{id}")
    public String deleteCurrency(@PathVariable int id) {
        Currency currency = currencyService.getById(id);
        currencyService.delete(currency);
        return "redirect:/currencies";
    }
}
