package com.example.controllers;

import com.example.dtos.mappers.UserTransMapper;
import com.example.dtos.transactionDTOs.UserTransactionDTO;
import com.example.models.User;
import com.example.models.transactions.UserTransaction;
import com.example.services.contracts.CurrencyService;
import com.example.services.contracts.UserService;
import com.example.services.contracts.transactionService.UserTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class UserTransactionController {
    private UserTransactionService userTransactionService;
    private UserService userService;
    private CurrencyService currencyService;
    private UserTransMapper mapper;

    @Autowired
    public UserTransactionController(UserTransactionService userTransactionService, UserService userService,
                                     CurrencyService currencyService, UserTransMapper mapper) {
        this.userTransactionService = userTransactionService;
        this.userService = userService;
        this.currencyService = currencyService;
        this.mapper = mapper;
    }

    @GetMapping("/userTransactions")
    public String showUserTransactions(Model model) {
        model.addAttribute("userTransactions", userTransactionService.getAll());
        return "userTransactions";
    }

    @GetMapping("/userTransactions/new")
    public String showNewUserTransactions(Model model) {
        model.addAttribute("userTransaction", new UserTransactionDTO());
        model.addAttribute("users", userService.getAll());
        model.addAttribute("currencies", currencyService.getAll());
        return "userTransaction";
    }

    @PostMapping("/userTransactions/new")
    public String createUserTransaction(@Valid @ModelAttribute("userTransaction")
                                                    UserTransactionDTO userTransactionDTO, BindingResult errors) {
        if (errors.hasErrors()){
            return "userTransaction";
        }
        UserTransaction userTransaction = new UserTransaction();
        userTransactionService.create(mapper.mapFromDtoToUserTransaction(userTransaction, userTransactionDTO));
        return "redirect:/userTransactions";
    }

    @PostMapping("/userTransactions/delete/{id}")
    public String deleteUserTransaction(@PathVariable int id) {
        UserTransaction userTransaction = userTransactionService.getById(id);
        userTransactionService.delete(userTransaction);

        return "redirect:/userTransactions";
    }
}
