package com.example.controllers;

import com.example.dtos.UserDetailsDTO;
import com.example.dtos.mappers.UserDetailsMapper;
import com.example.models.User;
import com.example.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;


@Controller
public class ProfileController {
    UserService userService;
    UserDetailsMapper mapper;

    @Autowired
    public ProfileController(UserService userService, UserDetailsMapper mapper) {
        this.userService = userService;
        this.mapper = mapper;
    }

    @GetMapping("/users-profile")
    public ModelAndView showUserProfile(Principal principal){
        ModelAndView mav = new ModelAndView("users-profile");
        mav.addObject("currentUser", userService.getUserByUsername(principal.getName()));
        return mav;
    }


    @GetMapping("/users-profile/updateDetails")
    public ModelAndView showUpdateDetailsForm(Principal principal) {
        User user = userService.getUserByUsername(principal.getName());
        ModelAndView mav = new ModelAndView("updateDetails");
        mav.addObject("details", mapper.fromUserDetailsToDto(user.getUserDetails()));

        return mav;
    }
    @PostMapping("/users-profile/updateDetails")
    public String updateUserDetails(@ModelAttribute UserDetailsDTO userDetailsDTO, Principal principal) {
        userService.updateUserDetails(userService.getUserByUsername(principal.getName()).getId(), userDetailsDTO);
        return "redirect:/users-profile";
    }
}
