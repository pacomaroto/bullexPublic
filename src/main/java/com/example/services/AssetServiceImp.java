package com.example.services;

import com.example.models.Asset;
import com.example.repository.contracts.AssetRepository;
import com.example.services.contracts.AssetService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AssetServiceImp implements AssetService {
    AssetRepository assetRepository;

    public AssetServiceImp(AssetRepository assetRepository) {
        this.assetRepository = assetRepository;
    }

    @Override
    public Asset getById(int id) {
        return assetRepository.getById(id);
    }

    @Override
    public List<Asset> getAll() {
        return assetRepository.getAll();
    }

    @Override
    public void create(Asset asset) {
        assetRepository.create(asset);
    }

    @Override
    public void update(Asset asset) {
        assetRepository.update(asset);
    }

    @Override
    public void delete(Asset asset) {
        assetRepository.delete(asset);
    }
}
