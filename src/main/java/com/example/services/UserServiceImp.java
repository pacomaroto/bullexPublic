package com.example.services;

import com.example.dtos.UserDetailsDTO;
import com.example.dtos.mappers.UserDetailsMapper;
import com.example.exceptions.EntityNotFoundException;
import com.example.models.User;
import com.example.models.UserDetails;
import com.example.repository.contracts.UserRepository;
import com.example.services.contracts.FileStorageService;
import com.example.services.contracts.UserService;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class UserServiceImp implements UserService {
    public static final String IMAGE_PATH_FOR_USER = "users/";
    public static final String DEFAULT_USER_IMAGE = "/images/users/default-avatar.png";
    private final UserRepository userRepository;
    private FileStorageService fileStorageService;
    private final UserDetailsMapper mapper;


    public UserServiceImp(UserRepository userRepository, FileStorageService fileStorageService, UserDetailsMapper mapper) {
        this.userRepository = userRepository;
        this.fileStorageService = fileStorageService;
        this.mapper = mapper;
    }

    @Override
    public User getById(int id) {
        if (userRepository.getById(id) == null) {
            throw new EntityNotFoundException("User", id);
        }
        return userRepository.getById(id);
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public void create(User user) {
        userRepository.create(user);
    }

    @Override
    public void update(User user) {
        userRepository.update(user);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }


    @Override
    public User getUserByUsername(String username) {
        return userRepository.getUserByUsername(username);
    }


    @Override
    public void addDefaultUserDetails(String username) {
        User user = userRepository.getUserByUsername(username);
        if (user == null) {
            throw new EntityNotFoundException("This user does not exist!");
        }
        UserDetails userDetails = new UserDetails("", "", 2, "", DEFAULT_USER_IMAGE, "a@abv.bg");
        userRepository.createUserDetails(user.getId(), userDetails);
    }

    @Override
    public void updateUserDetails(int id, UserDetailsDTO userDetailsDTO) {
        User user = userRepository.getById(id);
        if (user == null) {
            throw new EntityNotFoundException("This user does not exist!");
        }
        UserDetails userDetails = mapper.fromUserDetailsDtoToUserDetails(userDetailsDTO);
        userDetails.setId(user.getUserDetails().getId());
        userDetails.setPictureURL(getPictureUrl(userDetailsDTO, user.getUsername()));
        userRepository.updateUserDetails(userDetails);
    }

    private String getPictureUrl(UserDetailsDTO userDetailsDTO, String username) {
        if (userDetailsDTO.getFile() != null) {
            if (userDetailsDTO.getFile().isEmpty()) {
                return DEFAULT_USER_IMAGE;
            } else {
                return fileStorageService.uploadFile(userDetailsDTO.getFile(),
                        IMAGE_PATH_FOR_USER + username);
            }
        } else {
            return DEFAULT_USER_IMAGE;
        }
    }
}
