package com.example.services;

import com.example.models.Currency;
import com.example.repository.contracts.CurrencyRepository;
import com.example.services.contracts.CurrencyService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CurrencyServiceImp implements CurrencyService {
    CurrencyRepository currencyRepository;

    public CurrencyServiceImp(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @Override
    public Currency getById(int id) {
        return currencyRepository.getById(id);
    }

    @Override
    public List<Currency> getAll() {
        return currencyRepository.getAll();
    }

    @Override
    public void create(Currency currency) {
        currencyRepository.create(currency);
    }

    @Override
    public void update(Currency currency) {
        currencyRepository.update(currency);
    }

    @Override
    public void delete(Currency currency) {
        currencyRepository.delete(currency);
    }
}
