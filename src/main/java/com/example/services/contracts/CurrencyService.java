package com.example.services.contracts;
import com.example.models.Currency;

import java.util.List;

public interface CurrencyService {
    Currency getById(int id);

    List<Currency> getAll();

    void create(Currency currency);

    void update(Currency currency);

    void delete(Currency currency);
}
