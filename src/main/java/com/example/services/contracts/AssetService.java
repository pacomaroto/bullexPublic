package com.example.services.contracts;
import com.example.models.Asset;

import java.util.List;

public interface AssetService {
    Asset getById(int id);

    List<Asset> getAll();

    void create(Asset asset);

    void update(Asset asset);

    void delete(Asset asset);
}
