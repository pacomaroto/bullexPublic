package com.example.services.contracts;

import com.example.dtos.UserDetailsDTO;
import com.example.models.User;
import com.example.models.UserDetails;

import java.util.List;

public interface UserService {
    User getById(int id);

    List<User> getAll();

    void create(User user);

    void update(User user);

    void delete(User user);

    User getUserByUsername(String username);

    void updateUserDetails(int userId, UserDetailsDTO userDetailsDTO);

    void addDefaultUserDetails(String username);
}
