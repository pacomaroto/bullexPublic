package com.example.services.contracts.transactionService;
import com.example.models.transactions.UserTransaction;

import java.util.List;

public interface UserTransactionService{
    UserTransaction getById(int id);
    List<UserTransaction> getAll();
    void create(UserTransaction userTransaction);
    void update(UserTransaction userTransaction);
    void delete(UserTransaction userTransaction);
}
