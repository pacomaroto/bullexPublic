package com.example.services.contracts.transactionService;

import com.example.models.transactions.CurrencyExchange;

import java.util.List;

public interface CurrencyExchangeService {
    CurrencyExchange getById(int id);

    List<CurrencyExchange> getAll();
    void create(CurrencyExchange currencyExchange);
    void update(CurrencyExchange currencyExchange);
    void delete(CurrencyExchange currencyExchange);
}
