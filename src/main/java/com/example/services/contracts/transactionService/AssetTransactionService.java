package com.example.services.contracts.transactionService;

import com.example.models.transactions.AssetTransaction;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface AssetTransactionService {
    AssetTransaction getById(int id);

    List<AssetTransaction> getAll();
    void create(AssetTransaction assetTransaction);
    void update(AssetTransaction assetTransaction);
    void delete(AssetTransaction assetTransaction);
}
