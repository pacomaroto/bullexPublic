package com.example.services.contracts.transactionService;

import com.example.models.transactions.DividendTransaction;

import java.util.List;

public interface DividendTransactionService{
    DividendTransaction getById(int id);

    List<DividendTransaction> getAll();
    void create(DividendTransaction dividendTransaction);
    void update(DividendTransaction dividendTransaction);
    void delete(DividendTransaction dividendTransaction);
}
