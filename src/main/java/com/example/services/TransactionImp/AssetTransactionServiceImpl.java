package com.example.services.TransactionImp;

import com.example.models.transactions.AssetTransaction;
import com.example.repository.contracts.transactionRepository.AssetTransactionRepository;
import com.example.services.contracts.transactionService.AssetTransactionService;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class AssetTransactionServiceImpl implements AssetTransactionService {
    AssetTransactionRepository assetTransactionRepository;

    public AssetTransactionServiceImpl(AssetTransactionRepository assetTransactionRepository) {
        this.assetTransactionRepository = assetTransactionRepository;
    }


    @Override
    public AssetTransaction getById(int id) {
        return assetTransactionRepository.getById(id);
    }

    @Override
    public List<AssetTransaction> getAll() {
        return assetTransactionRepository.getAll();
    }

    @Override
    public void create(AssetTransaction assetTransaction) {
        assetTransactionRepository.create(assetTransaction);
    }

    @Override
    public void update(AssetTransaction assetTransaction) {
        assetTransactionRepository.update(assetTransaction);
    }

    @Override
    public void delete(AssetTransaction assetTransaction) {
        assetTransactionRepository.delete(assetTransaction);
    }
}
