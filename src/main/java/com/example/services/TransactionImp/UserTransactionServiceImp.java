package com.example.services.TransactionImp;
import com.example.models.transactions.UserTransaction;
import com.example.repository.contracts.transactionRepository.UserTransactionRepository;
import com.example.services.contracts.transactionService.UserTransactionService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserTransactionServiceImp implements UserTransactionService {
    UserTransactionRepository userTransactionRepository;

    public UserTransactionServiceImp(UserTransactionRepository userTransactionRepository) {
        this.userTransactionRepository = userTransactionRepository;
    }

    @Override
    public UserTransaction getById(int id) {
        return userTransactionRepository.getById(id);
    }

    @Override
    public List<UserTransaction> getAll() {
        return userTransactionRepository.getAll();
    }

    @Override
    public void create(UserTransaction userTransaction) {
        userTransactionRepository.create(userTransaction);
    }

    @Override
    public void update(UserTransaction userTransaction) {
        userTransactionRepository.create(userTransaction);
    }

    @Override
    public void delete(UserTransaction userTransaction) {
        userTransactionRepository.delete(userTransaction);
    }
}
