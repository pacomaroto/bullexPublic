package com.example.services.TransactionImp;

import com.example.models.transactions.DividendTransaction;
import com.example.repository.contracts.transactionRepository.DividendTransactionRepository;
import com.example.services.contracts.transactionService.DividendTransactionService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class DividendTransactionServiceImp implements DividendTransactionService {
    DividendTransactionRepository dividendTransactionRepository;

    public DividendTransactionServiceImp(DividendTransactionRepository dividendTransactionRepository) {
        this.dividendTransactionRepository = dividendTransactionRepository;
    }

    @Override
    public DividendTransaction getById(int id) {
        return dividendTransactionRepository.getById(id);
    }

    @Override
    public List<DividendTransaction> getAll() {
        return dividendTransactionRepository.getAll();
    }

    @Override
    public void create(DividendTransaction dividendTransaction) {
        dividendTransactionRepository.create(dividendTransaction);
    }

    @Override
    public void update(DividendTransaction dividendTransaction) {
        dividendTransactionRepository.update(dividendTransaction);
    }

    @Override
    public void delete(DividendTransaction dividendTransaction) {
        dividendTransactionRepository.delete(dividendTransaction);
    }

}
