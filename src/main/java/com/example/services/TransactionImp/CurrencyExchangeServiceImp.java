package com.example.services.TransactionImp;

import com.example.exceptions.EntityNotFoundException;
import com.example.models.transactions.CurrencyExchange;
import com.example.repository.contracts.transactionRepository.CurrencyExchangeRepository;
import com.example.services.contracts.transactionService.CurrencyExchangeService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CurrencyExchangeServiceImp implements CurrencyExchangeService {
    CurrencyExchangeRepository currencyExchangeRepository;

    public CurrencyExchangeServiceImp(CurrencyExchangeRepository currencyExchangeRepository) {
        this.currencyExchangeRepository = currencyExchangeRepository;
    }

    @Override
    public CurrencyExchange getById(int id) {
        return currencyExchangeRepository.getById(id);
    }

    @Override
    public List<CurrencyExchange> getAll() {
        if (currencyExchangeRepository.getAll().isEmpty()){
            throw new EntityNotFoundException("List is empty");
        }
        return currencyExchangeRepository.getAll();
    }

    @Override
    public void create(CurrencyExchange currencyExchange) {
        currencyExchangeRepository.create(currencyExchange);
    }

    @Override
    public void update(CurrencyExchange currencyExchange) {
        currencyExchangeRepository.update(currencyExchange);
    }

    @Override
    public void delete(CurrencyExchange currencyExchange) {
        currencyExchangeRepository.delete(currencyExchange);
    }
}
