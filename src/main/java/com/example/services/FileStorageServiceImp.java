package com.example.services;

import com.example.services.contracts.FileStorageService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;
//https://searchcode.com/file/281654146/src/main/java/com.startit.uploadfile/service/FileStorageService.java/
@Service
public class FileStorageServiceImp implements FileStorageService {
    @Value("C:/Users/Stivan/Desktop/bullex/src/main/webapp/WEB-INF/images")
    public String uploadDir;

    @Override
    public String uploadFile(MultipartFile file, String name) {
        String[] contents = file.getContentType().split("/");
        String content = contents[1];
        try {
            Path copyLocation = Paths
                    .get(uploadDir + File.separator + StringUtils.cleanPath(name + "." + content));
            Files.copy(file.getInputStream(), copyLocation, StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Could not store file " + file.getOriginalFilename()
                    + ". Please try again!");
        }

        return "/images/" + name + "." + content;
    }
}
