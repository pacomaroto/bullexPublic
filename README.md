---

# Bullex

## Description
Bullex is a fund management application that allows users to keep track of their investments, including dividends, shares, and currencies. The project aims to help users monitor their assets, view their purchase prices, and track the dividends they will receive. Currently, the project includes the architecture for creating, retrieving, updating, and deleting financial records.

## Features
- Manage dividends, shares, currencies, and other financial assets.
- Track purchase prices and received dividends.
- User authentication and security.
- CRUD operations for financial records.

## Installation

### Prerequisites
- Java 8
- Gradle
- MySQL

### Steps
1. **Clone the repository**
   ```bash
   git clone <repository-url>
   cd bullex
   ```

2. **Set up MySQL**
   Create a MySQL database and update the connection details in `application.properties`.

3. **Build the project**
   ```bash
   ./gradlew build
   ```

4. **Run the project**
   ```bash
   ./gradlew bootRun
   ```

## Usage

### Running the Application
Once the application is running, you can access it at `http://localhost:8080`.

### Example Usage
- Create, read, update, and delete financial records.
- View your portfolio and track your investments.

## Project Structure
The project follows a standard Spring Boot structure:

```
bullexPublic-master/
├── DB/
│   ├── bulexDiagram.mwb.bak
│   ├── bullexx.mwb
│   ├── bullexx.sql
├── src/
│   ├── main/
│   │   ├── java/
│   │   │   ├── com/
│   │   │   │   ├── example/
│   │   │   │   │   ├── bullex/
│   │   │   │   │   │   ├── BullexApplication.java
│   │   │   │   │   ├── configuration/
│   │   │   │   │   │   ├── HibernateConfig.java
│   │   │   │   │   │   ├── MyCustomLoginSuccessHandler.java
│   │   │   │   │   │   ├── ResourceWebConfig.java
│   │   │   │   │   │   ├── SecurityConfig.java
│   │   │   │   │   │   ├── ThymeleafConfig.java
│   │   │   │   │   ├── controllers/
│   │   │   │   │   │   ├── AssetsController.java
│   │   │   │   │   │   ├── AssetTransactionsController.java
│   │   │   │   │   │   ├── CurrenciesController.java
│   │   │   │   │   │   ├── CurrencyExchangesController.java
│   │   │   │   │   │   ├── DividendTransactionController.java
│   │   │   │   │   │   ├── errors/
│   │   │   │   │   │   │   ├── CustomControllerAdvice.java
│   │   │   │   │   │   │   ├── ErrorResponse.java
│   │   │   │   │   │   ├── HomeController.java
│   │   │   │   │   │   ├── LoginController.java
│   │   │   │   │   │   ├── ProfileController.java
│   │   │   │   │   │   ├── RegisterController.java
│   │   │   │   │   │   ├── rest/
│   │   │   │   │   │   │   ├── AssetRestController.java
│   │   │   │   │   │   │   ├── CurrencyRestController.java
│   │   │   │   │   │   │   ├── transaction/
│   │   │   │   │   │   │   │   ├── AssetTransactionRestController.java
│   │   │   │   │   │   │   │   ├── CurrencyExchangeRestController.java
│   │   │   │   │   │   │   │   ├── DividendTransactionRestController.java
│   │   │   │   │   │   │   │   ├── UserTransactionRestController.java
│   │   │   │   │   │   │   ├── UserRestController.java
│   │   │   │   │   │   ├── UsersController.java
│   │   │   │   │   │   ├── UserTransactionController.java
│   │   │   │   │   ├── dtos/
│   │   │   │   │   │   ├── AssetDTO.java
│   │   │   │   │   │   ├── CurrencyDTO.java
│   │   │   │   │   │   ├── mappers/
│   │   │   │   │   │   │   ├── AssetMapper.java
│   │   │   │   │   │   │   ├── AssetTransMapper.java
│   │   │   │   │   │   │   ├── CurrencyExchangeMapper.java
│   │   │   │   │   │   │   ├── DividendTransMapper.java
│   │   │   │   │   │   │   ├── UserDetailsMapper.java
│   │   │   │   │   │   │   ├── UserMapper.java
│   │   │   │   │   │   │   ├── UserRegisterMapper.java
│   │   │   │   │   │   │   ├── UserTransMapper.java
│   │   │   │   │   │   ├── transactionDTOs/
│   │   │   │   │   │   │   ├── AssetTransactionDTO.java
│   │   │   │   │   │   │   ├── CurrencyExchangeDTO.java
│   │   │   │   │   │   │   ├── DividendTransactionDTO.java
│   │   │   │   │   │   │   ├── UserTransactionDTO.java
│   │   │   │   │   │   ├── UserDetailsDTO.java
│   │   │   │   │   │   ├── UserDTO.java
│   │   │   │   │   │   ├── UserRegisterDTO.java
│   │   │   │   │   ├── exceptions/
│   │   │   │   │   │   ├── DuplicateEntityException.java
│   │   │   │   │   │   ├── EntityNotFoundException.java
│   │   │   │   │   ├── message/
│   │   │   │   │   │   ├── ResponseMessage.java
│   │   │   │   │   ├── models/
│   │   │   │   │   │   ├── Asset.java
│   │   │   │   │   │   ├── Currency.java
│   │   │   │   │   │   ├── enums/
│   │   │   │   │   │   │   ├── AssetType.java
│   │   │   │   │   │   │   ├── TransactionType.java
│   │   │   │   │   │   ├── FileInfo.java
│   │   │   │   │   │   ├── transactions/
│   │   │   │   │   │   │   ├── AssetTransaction.java
│   │   │   │   │   │   │   ├── CurrencyExchange.java
│   │   │   │   │   │   │   ├── DividendTransaction.java
│   │   │   │   │   │   │   ├── UserTransaction.java
│   │   │   │   │   │   ├── User.java
│   │   │   │   │   │   ├── UserDetails.java
│   │   │   │   │   ├── repository/
│   │   │   │   │   │   ├── AssetRepositoryImp.java
│   │   │   │   │   │   ├── contracts/
│   │   │   │   │   │   │   ├── AssetRepository.java
│   │   │   │   │   │   │   ├── CurrencyRepository.java
│   │   │   │   │   │   │   ├── transactionRepository/
│   │   │   │   │   │   │   │   ├── AssetTransactionRepository.java
│   │   │   │   │   │   │   │   ├── CurrencyExchangeRepository.java
│   │   │   │   │   │   │   │   ├── DividendTransactionRepository.java
│   │   │   │   │   │   │   │   ├── UserTransactionRepository.java
│   │   │   │   │   │   │   ├── UserRepository.java
│   │   │   │   │   │   ├── CurrencyRepositoryImp.java
│   │   │   │   │   │   ├── transactionImp/
│   │   │   │   │   │   │   ├── AssetTransactionRepositoryImp.java
│   │   │   │   │   │   │   ├── CurrencyExchangeRepositoryImp.java
│   │   │   │   │   │   │   ├── DividendTransactionRepositoryImp.java
│   │   │   │   │   │   │   ├── UserTransactionRepositoryImp.java
│   │   │   │   │   │   ├── UserRepositoryImp.java
│   │   │   │   │   ├── services/
│   │   │   │   │   │   ├── AssetServiceImp.java
│   │   │   │   │   │   ├── contracts/
│   │   │   │   │   │   │   ├── AssetService.java
│   │   │   │   │   │   │   ├── CurrencyService.java
│   │   │   │   │   │   │   ├── FileStorageService.java
│   │   │   │   │   │   │   ├── transactionService/
│   │   │   │   │   │   │   │   ├── AssetTransactionService.java
│   │   │   │   │   │   │   │   ├── CurrencyExchangeService.java
│   │   │   │   │   │   │   │   ├── DividendTransactionService.java
│   │   │   │   │   │   │   │   ├── UserTransactionService.java
│   │   │   │   │   │   │   ├── UserService.java
│   │   │   │   │   │   ├── CurrencyServiceImp.java
│   │   │   │   │   │   ├── FileStorageServiceImp.java
│   │   │   │   │   │   ├── TransactionImp/
│   │   │   │   │   │   │   ├── AssetTransactionServiceImpl.java
│   │   │   │   │   │   │   ├── CurrencyExchangeServiceImp.java
│   │   │   │   │   │   │   ├── DividendTransactionServiceImp.java
│   │   │   │   │   │   │   ├── UserTransactionServiceImp.java
│   │   │   │   │   │   ├── UserServiceImp.java
│   │   ├── resources/
│   │   │   ├── application.properties
│   │   │   ├── messages.properties
│   │   │   ├── static/
│   │   │   │   ├── css/
│   │   │   │   │   ├── bootstrap-grid.css
│   │   │   │   │   ├── bootstrap-grid.css.map
│   │   │   │   │   ├── bootstrap-grid.min.css
│   │   │   │   │   ├── bootstrap-grid.min.css.map
│   │   │   │   │   ├── bootstrap-grid.rtl.css
│   │   │   │   │   ├── bootstrap-grid.rtl.css.map
│   │   │   │   │   ├── bootstrap-grid.rtl.min.css
│   │   │   │   │   ├── bootstrap-grid.rtl.min.css.map
│   │   │   │   │   ├── bootstrap-reboot.css
│   │   │   │   │   ├── bootstrap-reboot.css.map
│   │   │   │   │   ├── bootstrap-reboot.min.css
│   │   │   │   │   ├── bootstrap-reboot.min.css.map
│   │   │   │   │   ├── bootstrap-reboot.rtl.css
│   │   │   │   │   ├── bootstrap-reboot.rtl.css.map
│   │   │   │   │   ├── bootstrap-reboot.rtl.min.css
│   │   │   │   │   ├── bootstrap-reboot.rtl.min.css.map
│   │   │   │   │   ├── bootstrap-utilities.css
│   │   │   │   │   ├── bootstrap-utilities.css.map
│   │   │   │   │   ├── bootstrap-utilities.min.css
│   │   │   │   │   ├── bootstrap-utilities.min.css.map
│   │   │   │   │   ├── bootstrap-utilities.rtl.css
│   │   │   │   │   ├── bootstrap-utilities.rtl.css.map
│   │   │   │   │   ├── bootstrap-utilities.rtl.min.css
│   │   │   │   │   ├── bootstrap-utilities.rtl.min.css.map
│   │   │   │   │   ├── bootstrap.css
│   │   │   │   │   ├── bootstrap.css.map
│   │   │   │   │   ├── bootstrap.min.css
│   │   │   │   │   ├── bootstrap.min.css.map
│   │   │   │   │   ├── bootstrap.rtl.css
│   │   │   │   │   ├── bootstrap.rtl.css.map
│   │   │   │   │   ├── bootstrap.rtl.min.css
│   │   │   │   │   ├── bootstrap.rtl.min.css.map
│   │   │   │   │   ├── style.css
│   │   │   │   ├── img/
│   │   │   │   │   ├── 1660746037585-file.JPG
│   │   │   │   │   ├── 1660748068296-file.JPG
│   │   │   │   │   ├── apple-touch-icon.png
│   │   │   │   │   ├── card.jpg
│   │   │   │   │   ├── default-avatar.png
│   │   │   │   │   ├── favicon.png
│   │   │   │   │   ├── logo.png
│   │   │   │   │   ├── messages-1.jpg
│   │   │   │   │   ├── messages-2.jpg
│   │   │   │   │   ├── messages-3.jpg
│   │   │   │   │   ├── news-1.jpg
│   │   │   │   │   ├── news-2.jpg
│   │   │   │   │   ├── news-3.jpg
│   │   │   │   │   ├── news-4.jpg
│   │   │   │   │   ├── news-5.jpg
│   │   │   │   │   ├── not-found.svg
│   │   │   │   │   ├── product-1.jpg
│   │   │   │   │   ├── product-2.jpg
│   │   │   │   │   ├── product-3.jpg
│   │   │   │   │   ├── product-4.jpg
│   │   │   │   │   ├── product-5.jpg
│   │   │   │   │   ├── profile-img.jpg
│   │   │   │   │   ├── profilepic.JPG
│   │   │   │   │   ├── slides-1.jpg
│   │   │   │   │   ├── slides-2.jpg
│   │   │   │   │   ├── slides-3.jpg
│   │   │   │   ├── js/
│   │   │   │   │   ├── bootstrap.bundle.js
│   │   │   │   │   ├── bootstrap.bundle.js.map
│   │   │   │   │   ├── bootstrap.bundle.min.js
│   │   │   │   │   ├── bootstrap.bundle.min.js.map
│   │   │   │   │   ├── bootstrap.esm.js
│   │   │   │   │   ├── bootstrap.esm.js.map
│   │   │   │   │   ├── bootstrap.esm.min.js
│   │   │   │   │   ├── bootstrap.esm.min.js.map
│   │   │   │   │   ├── bootstrap.js
│   │   │   │   │   ├── bootstrap.js.map
│   │   │   │   │   ├── bootstrap.min.js
│   │   │   │   │   ├── bootstrap.min.js.map
│   │   │   │   │   ├── login.js
│   │   │   │   │   ├── main.js
│   │   │   │   ├── style.css
│   │   │   │   │   │   ├── README.md
│   │   │   ├── templates/
│   │   │   │   ├── access-denied.html
│   │   │   │   ├── asset.html
│   │   │   │   ├── assets.html
│   │   │   │   ├── assetTransaction.html
│   │   │   │   ├── assetTransactions.html
│   │   │   │   ├── changelog.txt
│   │   │   │   ├── charts-apexcharts.html
│   │   │   │   ├── charts-chartjs.html
│   │   │   │   ├── charts-echarts.html
│   │   │   │   ├── components-accordion.html
│   │   │   │   ├── components-alerts.html
│   │   │   │   ├── components-badges.html
│   │   │   │   ├── components-breadcrumbs.html
│   │   │   │   ├── components-buttons.html
│   │   │   │   ├── components-carousel.html
│   │   │   │   ├── components-list-group.html
│   │   │   │   ├── components-modal.html
│   │   │   │   ├── components-pagination.html
│   │   │   │   ├── components-progress.html
│   │   │   │   ├── currencies.html
│   │   │   │   ├── currency.html
│   │   │   │   ├── currencyExchange.html
│   │   │   │   ├── currencyExchanges.html
│   │   │   │   ├── dividendTransaction.html
│   │   │   │   ├── dividendTransactions.html
│   │   │   │   ├── forms-editors.html
│   │   │   │   ├── forms-elements.html
│   │   │   │   ├── forms-layouts.html
│   │   │   │   ├── forms-validation.html
│   │   │   │   ├── header.html
│   │   │   │   ├── icons-bootstrap.html
│   │   │   │   ├── icons-boxicons.html
│   │   │   │   ├── icons-remix.html
│   │   │   │   ├── images/
│   │   │   │   │   ├── logo.png
│   │   │   │   │   ├── OKYT1318.JPEG
│   │   │   │   ├── index.html
│   │   │   │   ├── pages-blank.html
│   │   │   │   ├── pages-contact.html
│   │   │   │   ├── pages-error-404.html
│   │   │   │   ├── pages-faq.html
│   │   │   │   ├── pages-login.html
│   │   │   │   ├── pages-register.html
│   │   │   │   ├── Readme.txt
│   │   │   │   ├── sidebar.html
│   │   │   │   ├── tables-data.html
│   │   │   │   ├── tables-general.html
│   │   │   │   ├── updateDetails.html
│   │   │   │   ├── user.html
│   │   │   │   ├── users-profile.html
│   │   │   │   ├── users.html
│   │   │   │   ├── userTransaction.html
│   │   │   │   ├── userTransactions.html
│   │   ├── webapp/
│   │   │   ├── WEB-INF/
│   │   │   │   ├── images/
│   │   │   │   │   ├── others/
│   │   │   │   │   │   ├── logo.png
│   │   │   │   │   ├── users/
│   │   │   │   │   │   ├── Boris.jpeg
│   │   │   │   │   │   ├── default-avatar.png
│   │   │   │   │   │   ├── Stivan.jpeg
│   ├── test/
│   │   ├── java/
│   │   │   ├── com/
│   │   │   │   ├── example/
│   │   │   │   │   ├── bullex/
│   │   │   │   │   │   ├── BullexApplicationTests.java

```

### Main Components
- **config**: Configuration classes for the application.
- **controllers**: REST controllers for handling HTTP requests.
- **models**: Entity classes representing the database schema.
- **repositories**: Repository interfaces for data access.
- **services**: Service classes containing business logic.

## Dependencies
The project uses the following dependencies:
- Spring Boot
- Spring Data JPA
- Hibernate
- Spring Security
- Thymeleaf
- MySQL Connector

For a full list of dependencies, refer to the `build.gradle` file.

---